**[《能创造比特币就必定能创造另一种加密货币，加密货币多了，是否意味着未来加密货币会蹦盘？》](https://www.zhihu.com/question/62633132/answer/202245315)**

铸币税的英文为 Seigniorage，是从法语 Seigneur（封建领主、君主、诸侯）演变而来的，又称铸币利差。《美国传统词典》进一步将其解释为通过铸造硬币所获得的收益或利润，通常是指所使用的贵金属内含值与硬币面值之差。因此，铸币税并不是国家通过权力征收的一种税赋，而是铸造货币所得到的特殊收益。

> 货币的本质是信任。
>
> 比特币这种货币没有中心化的信任衡量机构，比特币的价值，也就是其“信用体系”，来源于对于中心化的单位的不信任。每个国家都发现铸币税这玩意太好收了，饮鸠止渴，导致国与国之间信任度越来越差，甚至各国人民自己都对于国家的货币产生不信任。
>
> 而信任就和质量一样，是守恒的，对于中心化货币的信任减少，对于去中心化的货币信任度自然增加。
>
> 这就是比特币价值不断提升的本质原因。所以对于数字货币比如比特币来说，只有2个结局，要么各国想明白了不再超发货币，按照科学合理的方式增发货币，各国统一稳定的汇率，维持住中心化货币的信用体系，那么比特币这类的货币价值就会无限接近0。要么各国之间继续饮鸠止渴，更为疯狂地用超发货币的方法来向所有人收铸币税，那么对于中心化货币的信任会越来越低，比特币这种去中心化的货币总价值会无限接近于所有对政府，以及其他政府不信任，但又需要进行国际贸易的单位的劳动生产总值。

<!-- -->
> 也就是说，老王必须付出极其高昂的成本，才能改变现有的数字货币的生态，让自己的货币产生冲击市场的可能性。
>
> 抢占对自己有利的数字货币的流通性，就是抢占下一个时代的铸币税。


自注：原本的货币体系受到冲击和约束后可能会崩塌，选择数字加密货币就是选择未来。


**[《比特币的价格是怎样定出来的？》](https://www.zhihu.com/question/21458570)**

比特币价格是由供需决定的。

**[《I’ve finally accepted that I am a failure at crypto trading》](https://www.reddit.com/r/CryptoCurrency/comments/7owfga/ive_finally_accepted_that_i_am_a_failure_at/)**

> you learned some valuable lessons and you learned them early in life without devasting consequences. congrats.
>
> - understand that playing with a single coin (or stock or w/e), is the single riskiest thing you can do as an investor.
> - look into warren buffet and some of his investing tips...passive investing is what will earn you money, without the stress. ironic because buffet doesnt like crypto but hes old and doesnt understand it so ill let that slide...
> - you can try to replicate passive investing in the crypto world by have a diversified portfolio of more than 5 coins...
> - at the very simplest...anybody could could put money into the top 10 by market cap and just hold.
> - perhaps change your mindset a bit...from JUST trying to make money, to being genuinely interested in the tech, and feel as if you are supporting something that you believe in that has a future that you are helping to bring about


**[《Are ethereum and other protocols already 10x over-valued?》](https://s3.eu-west-2.amazonaws.com/john-pfeffer/An+Investor's+Take+on+Cryptoassets+v6.pdf)**

reddit 上面看到的一篇文章，待读

**[《主动投资和被动投资，具体是什么意思？》](https://www.zhihu.com/question/25017206)**

**[《》]()**
**[《》]()**
**[《》]()**
**[《》]()**
**[《》]()**
**[《》]()**
**[《》]()**
**[《》]()**
**[《》]()**
**[《》]()**
**[《》]()**
**[《》]()**
**[《》]()**
**[《》]()**
**[《》]()**
**[《》]()**